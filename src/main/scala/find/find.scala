package find

import find.cs214.Entry

def findLazy(entry: cs214.Entry, predicate: Entry => Boolean): LazyList[Entry] =
   ???

def findAndPrint(entry: cs214.Entry, predicate: cs214.Entry => Boolean): Boolean =
  ???

def findAllAndPrint(entry: cs214.Entry): Boolean =
  findAndPrint(entry, _ => true)

def findByNameAndPrint(entry: cs214.Entry, name: String): Boolean =
  findAndPrint(entry, _.name() == name)

def findBySizeEqAndPrint(entry: cs214.Entry, size: Long): Boolean =
  findAndPrint(entry, e => !e.isDirectory() && e.size() == size)

def findBySizeGeAndPrint(entry: cs214.Entry, minSize: Long): Boolean =
  findAndPrint(entry, e => !e.isDirectory() && e.size() >= minSize)

def findEmptyAndPrint(entry: cs214.Entry): Boolean =
  def isEmpty(e: cs214.Entry): Boolean =
    if e.isDirectory() then
      !e.hasChildren()
    else
      e.size() == 0

  findAndPrint(entry, isEmpty)

def findFirstByNameAndPrint(entry: cs214.Entry, name: String): Boolean =
  ???
